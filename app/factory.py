from flask import Flask
from celery import Celery
from flask_restful import Api
from flask_cors import CORS

from setting.config import CONFIGS


def create_app(register_stuffs=True, config=CONFIGS):
    app = Flask(__name__)
    app.config.from_object(config)
    app.config['CELERY_BROKER_URL']     = config.CELERY_BROKER_URL
    app.config['CELERY_RESULT_BACKEND'] = config.CELERY_RESULT_BACKEND
    return app

def get_celery_app_instance(app):
    celery = Celery(
        app.import_name,
        backend=app.config['CELERY_BROKER_URL'],
        broker=app.config['CELERY_BROKER_URL']
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


def register_rest_api(app):
    from app.api_handlers.urls_handler import GetMonitorURLs, FetchURLsStatus

    version = app.config["VERSION"]
    rest_api_url = app.config["REST_API_URL"]
    api_version_url = "/{}/{}/".format(rest_api_url, version)
    api = Api(app)
    CORS(app)

    api.add_resource(GetMonitorURLs, "{}urls/list".format(api_version_url))
    api.add_resource(FetchURLsStatus, "{}urls/status".format(api_version_url))
    ## TODO: Add more APIs up to the features of the application
    ## For eg: Update content requirements