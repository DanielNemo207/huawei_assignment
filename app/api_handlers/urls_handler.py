import json

from flask import jsonify
from flask_restful import Resource
from flask_restful.reqparse import RequestParser

from setting.config import CONFIGS as cf
from data_layer.postgresql_db import PostgreSQL_DB


class UrlsBaseHandler(Resource):

    def __init__(self):
        super().__init__()
        self.read_urls_from_config()
        self.db = PostgreSQL_DB()

    def read_urls_from_config(self):
        with open(cf.URLS_CONFIG_FILE) as file:
            self.urls = json.load(file)

class GetMonitorURLs(UrlsBaseHandler):
    '''
    API to fetch all URLS in config file
    '''

    def __init__(self):
        super().__init__()

    def get(self):
        return jsonify({'urls': self.urls})

class FetchURLsStatus(UrlsBaseHandler):
    '''
    API to fetch all URLS in config file
    '''

    def __init__(self):
        super().__init__()

    def get(self):
        url_status = self.db.fetch_urls_latest_status()
        return jsonify({'url_status': url_status})

## TODO: API for CRUD urls if needed
## For eg: Update content_requirements