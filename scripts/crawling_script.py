import os
import re
import time
import json
import logging
import argparse
import requests

from setting.config import CONFIGS as cf

def get_urls(config_file):
    with open(config_file) as file:
        urls = json.load(file)
    return urls

def validate_web_content(url, content, logging, requirments={}):
    validated_results = []
    for req in requirments:
        if req.get('rule') == 'include':
            include_type = req.get('args', {}).get('type', 'string')
            include_value = req.get('args', {}).get('value', '')
            if include_type == 'string':
                logging.info('URL: {}, Requirement: {}, is_OK: {}'.format(
                    url, req.get('desc', '<undefined>'), include_value in content)
                )
            elif include_type == 'regex':
                logging.info('URL: {}, Requirement: {}, is_OK: {}'.format(
                    url, req.get('desc', '<undefined>'), re.search(include_value, content) is not None)
                )
        else:
            logging.info('URL: {}, Requirement: {} has not supported rule type!'.format(
                url, req.get('desc', '<undefined>'))
            )
    return validated_results

def check_url(url, requirements, logging):
    resp = requests.get(url)
    status_code = resp.status_code
    web_content = resp.text
    elapsed_time = resp.elapsed.total_seconds()
    logging.info('URL: {}, Status: {}, Elapsed_time(sec): {}'.format(url, status_code, elapsed_time))
    if status_code == 200:
        validate_web_content(url, web_content, logging, requirements)
    
def crawl_urls(urls_config_file=cf.URLS_CONFIG_FILE):
    log_dir = os.path.join(os.path.normpath(os.getcwd()), 'logs')
    log_fname = os.path.join(log_dir, 'crawl_urls.log')
    logging.basicConfig(
        filename=log_fname,
        level=logging.DEBUG,
        format='%(asctime)s [%(levelname)s] %(message)s',
    )

    urls = get_urls(urls_config_file)
    while True:
        for url in urls:
            check_url(url, urls[url].get('content_requirments', []), logging)
        logging.info('Sleeping...')
        time.sleep(cf.URL_CRAWLING_TIME_INTERVAL)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--urls_config_file', type=str, default=cf.URLS_CONFIG_FILE)
    args = parser.parse_args()

    crawl_urls(args.urls_config_file)