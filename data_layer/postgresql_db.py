import sqlalchemy as db

from os.path import join, abspath, dirname
from setting.config import CONFIGS as cf

##TODO: Setup DB for status of monitoring URLs if needed

class PostgreSQL_DB():

    def __init__(self):
        self.connect()

    def connect(self):
        '''
        Connect to the database
        '''
        try:
            ## TODO: Connect to the DB
            engine = db.create_engine('dialect+driver://user:pass@host:port/db')
            connection = engine.connect()
        except Exception as e:
            print(e)

    def save_urls_status(self, status):
        '''
        Update a latest status of URL into DB
        '''
        try:
            ## TODO: Write SQL code / Maybe use sqlalchemy to save data row
            return True
        except Exception as e:
            print('[Error]', e)
            return False

    def fetch_urls_latest_status(self):
        '''
        Fetch a latest status of URL from DB
        '''
        try:
            ## TODO: Write SQL code / Maybe use sqlalchemy to select data row
            return True
        except Exception as e:
            print('[Error]', e)
            return False