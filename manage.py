import os
# from celery import uuid
from flask import request
from flask_script import Manager

from app.factory import create_app, get_celery_app_instance
from scripts.crawling_script import crawl_urls


app = create_app()
manager = Manager(app)
port = int(os.environ.get("PORT", 5001))
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

celery = get_celery_app_instance(app)

@celery.task(bind=True, name="manage.background_check_urls")
def background_check_urls(self):
    crawl_urls()

@app.route("/start_background_urls_crawling")
def start_background_urls_crawling():
    # task_id = uuid()
    task = background_check_urls.delay()
    return "Task {} Started URLs crawling script in background! Check celery terminal to see the logs!".format(task)

@app.route("/terminate_background_urls_crawling")
def terminate_background_urls_crawling():
    celery.control.revoke(request.args.get('task_id'), terminate = True, signal='SIGQUIT')
    celery.control.terminate(request.args.get('task_id'))
    # celery.control.shutdown()
    return f"Terminated URLs crawling script in background!"

##TODO Add any needed backgroud tasks here!

@manager.command
def server(crawling_script_only=False):
    if crawling_script_only:
        crawl_urls()
    else:
        app.run(debug=True, host='0.0.0.0', port=port)

if __name__ == '__main__':
    manager.run()
    