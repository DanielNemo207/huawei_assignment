# HUAWEI SENIOR SOFTWARE ENGINEER ASSIGNMENT
This is the brief architecture of the WEB CRAWLER APP
Main idea:
- The crawling script to check the status and content requirement of the URLs is allocated in "scripts". The log will allocated in "logs".
- For original purpose to periodically crawl the urls, it is enough to run the script alone
- For the scope of a monitor web app (which BE should be able to proccess requests, for eg updating monitoring URLs and their content requirements by APIs instead of changing the config file), the crawling script can be started/terminated as a background task. The starting and terminating of the script can be controlled by the App backend APIs.

## Quickstart

Create virtualenv (note: this project requires you to have python3.6+):

```
python3 -m venv ../venv
```

Install dependencies:

```
../venv/bin/pip install -r requirements.txt
```

Run app server to serve crawling script only:

```
../venv/bin/python manage.py server --crawling_script_only
```

Run app server to serve Backend with the crawling script running in background:

```
screen 1: ../venv/bin/python manage.py server
screen 2: ../venv/bin/redis-server
screen 3 (Optional --- if you want to run background script with celery task):
../venv/bin/celery -A manage.celery worker --loglevel=info --logfile=logs/crawl_urls.log
```

Run crawling script in background by 2 approaches:

1, Open another screen to run natively the script 
```
python -m scripts.crawling_script --urls_config_file=<FILE_PATH> (default FILE_PATH is in setting.config.py)
```

2, Start/Terminate Celery tasks for the script by APIs
```
/start_background_urls_crawling
/terminate_background_urls_crawling?task_id=<task_id>
```


## Subdirectories

* **app**: Contains all app's features
* **app/api_handler**: Contains all app's API hanlders
* **app/utils**: Contains all utility functions for the app and scripts
* **data_layer**: Contains all database handlers, add new db interface if needed
* **logs**: Contains all app's and scripts' log files
* **scripts**: Contains all scripts beside the main app log files
* **manage.py**: Flask manager for managing commands