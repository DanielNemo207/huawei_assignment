import re

from os import pardir
from os.path import join, abspath, dirname

# Base directories
CURRENT_DIR = abspath(dirname(__file__))
APP_DIR = abspath(join(CURRENT_DIR, pardir))
SETTING_DIR = abspath(join(APP_DIR, "setting"))
 

class BaseConfig(object):
    VERSION = "v1"
    REST_API_URL = "api"
    URL_REGEX = re.compile(
        r'^(?:http|ftp)s?://'  # http:// or https://
        # domain...
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
        r'(?::\d+)?'  # optional port
    )
    URLS_CONFIG_FILE = abspath(join(CURRENT_DIR, "urls_config.json"))
    URL_CRAWLING_TIME_INTERVAL = 60 * 10 # seconds (10 min)
    URL_CONTENT_VALIDATE_RULE_KEYS = ['include', 'not_include']

    CELERY_BROKER_URL = "redis://localhost:6379/0"
    CELERY_RESULT_BACKEND = "redis://localhost:6379/0"

CONFIGS = BaseConfig()
